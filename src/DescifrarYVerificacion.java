import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class DescifrarYVerificacion {
    public String algoritmo = "RSA";
    public PublicKey publicKey;
    public PrivateKey privateKey;
    public byte[] mensajeDescifrado;
    public byte[] mensajeCifrado;
    public byte[] firma;

    public void DescifrarYVerificar(){
        try {

            obtenerClaves();

            //obtener mensaje y firma
            FileInputStream fis=new FileInputStream("mensajeCifrado");
            mensajeCifrado= new byte[fis.available()];
            fis.read(mensajeCifrado);

            FileInputStream fis2=new FileInputStream("firmaMensaje");
            firma= new byte[fis2.available()];
            fis2.read(mensajeCifrado);

            fis.close();
            fis2.close();


            //descifrar mensaje
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, this.publicKey);
            this.mensajeDescifrado=cipher.doFinal(this.mensajeCifrado);

            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initVerify(publicKey);
            sig.update(mensajeDescifrado);
            boolean firmaValida = sig.verify(firma);

            if (firmaValida) {
                System.out.println("La firma es válida.");
            } else {
                System.out.println("La firma no es válida.");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
    private void obtenerClaves(){
        try {

            FileInputStream fis = new FileInputStream("clavePrivada");
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            KeyFactory kf = KeyFactory.getInstance(algoritmo);
            PKCS8EncodedKeySpec intercambiador = new PKCS8EncodedKeySpec(bytes);
            this.privateKey = kf.generatePrivate(intercambiador);
            fis.close();

            FileInputStream fis2 = new FileInputStream("clavePublica");
            byte[] bytes2 = new byte[fis2.available()];
            fis2.read(bytes2);
            KeyFactory kf2 = KeyFactory.getInstance(algoritmo);
            X509EncodedKeySpec intercambiador2 = new X509EncodedKeySpec(bytes2);
            this.publicKey = kf2.generatePublic(intercambiador2);
            fis2.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
