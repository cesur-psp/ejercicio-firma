import javax.crypto.Cipher;
import java.io.*;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

public class CifradoYFirma {
    public String algoritmo = "RSA";
    public PrivateKey privateKey;
    public PublicKey publicKey;
    public String mensaje;
    public byte[] mensajeCifrado;
    public byte[] firma;

    public void cifrarMensajeConFirma() {
        try {
        Scanner sc = new Scanner(System.in);

        obtenerClaves();


            System.out.println("Escribe tu mensaje:");
            this.mensaje = sc.nextLine();
            //cifrar mensaje
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, this.privateKey);
            this.mensajeCifrado = cipher.doFinal(mensaje.getBytes());


            //firmar mensaje
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initSign(this.privateKey);
            sig.update(this.mensaje.getBytes());
            this.firma = sig.sign();


            FileOutputStream fos=new FileOutputStream("mensajeCifrado");
            fos.write(this.mensajeCifrado);

            FileOutputStream fos2=new FileOutputStream("firmaMensaje");
            fos2.write(this.firma);

            fos.close();
            fos2.close();

            System.out.println("Mensaje cifrado y firmado correctamente");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void obtenerClaves(){
        try {

            FileInputStream fis = new FileInputStream("clavePrivada");
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            KeyFactory kf = KeyFactory.getInstance(algoritmo);
            PKCS8EncodedKeySpec intercambiador = new PKCS8EncodedKeySpec(bytes);
            this.privateKey = kf.generatePrivate(intercambiador);
            fis.close();

            FileInputStream fis2 = new FileInputStream("clavePublica");
            byte[] bytes2 = new byte[fis2.available()];
            fis2.read(bytes2);
            KeyFactory kf2 = KeyFactory.getInstance(algoritmo);
            X509EncodedKeySpec intercambiador2 = new X509EncodedKeySpec(bytes2);
            this.publicKey = kf2.generatePublic(intercambiador2);
            fis2.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
