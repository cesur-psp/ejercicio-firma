import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;

public class GeneradorClaves {
    public String algoritmo="RSA";
    public PublicKey publicKey;
    public PrivateKey privateKey;

    public GeneradorClaves() {
        try {
            KeyPairGenerator keygen;
            keygen = KeyPairGenerator.getInstance(algoritmo);
            KeyPair keyPair = keygen.genKeyPair();

            publicKey = keyPair.getPublic();
            privateKey = keyPair.getPrivate();

        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }

    }

    public void generar() {
        try {
            byte[] pk = publicKey.getEncoded();
            byte[] pk2 = privateKey.getEncoded();
            //fichero con clave publica
            FileOutputStream fos = new FileOutputStream("clavePublica");
            fos.write(pk);
            fos.close();
            //fichero con clave privada
            fos = new FileOutputStream("clavePrivada");
            fos.write(pk2);
            fos.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}